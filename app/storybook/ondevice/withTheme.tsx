/**
 * File: /storybook/ondevice/withTheme.tsx
 * Project: @native-theme-ui/core
 * File Created: 02-07-2022 13:20:19
 * Author: Clay Risser
 * -----
 * Last Modified: 02-11-2022 05:30:31
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* eslint-disable @typescript-eslint/no-explicit-any */

import React, { ReactNode } from 'react';
import * as Linking from 'expo-linking';
import { makeDecorator, LegacyStoryFn, StoryWrapper } from '@storybook/addons';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { themes } from '../../theme';
import { GlobalProvider } from 'app/provider';

const logger = console;
const Stack = createNativeStackNavigator();
const options = {
  themeName: 'main',
  autoContrast: 'A',
};
const theme = {
  ...(themes as any)[options.themeName],
  autoContrast: options.autoContrast,
};
logger.info('theme', theme);

const withTheme = makeDecorator({
  name: 'withTheme',
  parameterName: 'theme',
  skipIfNoParametersOrOptions: false,
  wrapper: ((getStory: LegacyStoryFn<ReactNode>, context: any) => {
    return (
      <GlobalProvider theme={theme} noNavigation>
        <NavigationContainer
          linking={{
            prefixes: [Linking.createURL('/')],
            config: {
              initialRouteName: 'story',
              screens: { story: '' },
            },
          }}
        >
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen name="story">{() => <>{getStory(context)}</>}</Stack.Screen>
          </Stack.Navigator>
        </NavigationContainer>
      </GlobalProvider>
    );
  }) as StoryWrapper,
});

export default withTheme;
