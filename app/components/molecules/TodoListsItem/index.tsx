/**
 * File: /components/molecules/TodoListsItem/index.tsx
 * Project: app
 * File Created: 26-09-2022 06:50:01
 * Author: Ajith Kumar
 * -----
 * Last Modified: 02-12-2022 15:19:37
 * Modified By: K S R P BHUSHAN
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Text, Box } from '@native-theme-ui/core';
import { useAnimationStateSx } from '@native-theme-ui/core';
interface TodoListsItemProps {
  children: string;
  deleteList: () => void;
}
export const TodoListsItem: FC<TodoListsItemProps> = (props: TodoListsItemProps) => {
  const animationState = useAnimationStateSx({
    in: {
      color: '#000000',
      backgroundColor: '#FFD700',
    },
    out: {
      color: '#ffffff',
      backgroundColor: '#1F1F1E',
    },
  });

  return (
    <Box
      state={animationState}
      onLongPress={() => props.deleteList()}
      onHoverIn={() => animationState.transitionTo('in')}
      onHoverOut={() => animationState.transitionTo('out')}
      fromSx={{ scale: 0 }}
      animateSx={{ scale: 1 }}
      transition={{ type: 'timing', duration: 300 }}
      style={{
        padding: 10,
        borderStyle: 'dashed',
        borderColor: 'gray',
        borderWidth: 12,
        borderRadius: 40,
        marginTop: 10,
        fontFamily: 'Georgia',
      }}
    >
      <Text>{props.children}</Text>
    </Box>
  );
};
