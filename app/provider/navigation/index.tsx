/**
 * File: /provider/navigation/index.tsx
 * Project: -
 * File Created: 18-08-2022 04:34:42
 * Author: Clay Risser
 * -----
 * Last Modified: 08-11-2022 03:13:17
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { ProviderProps } from '../index';

export const NavigationProvider: FC<ProviderProps> = ({ children }: ProviderProps) => <>{children}</>;
