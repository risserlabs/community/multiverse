/**
 * File: /provider/state.tsx
 * Project: app
 * File Created: 15-11-2022 10:05:52
 * Author: Clay Risser
 * -----
 * Last Modified: 28-11-2022 14:15:23
 * Modified By: Clay Risser
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { Suspense, FC } from 'react';
import { RecoilRoot } from 'recoil';
import ReactNativeRecoilPersist, { ReactNativeRecoilPersistGate } from 'react-native-recoil-persist';
import { ProviderProps } from './index';

export type StateProviderProps = ProviderProps;

export const StateProvider: FC<StateProviderProps> = ({ children }: StateProviderProps) => {
  return (
    <RecoilRoot>
      <ReactNativeRecoilPersistGate store={ReactNativeRecoilPersist}>
        <Suspense fallback={null}>{children}</Suspense>
      </ReactNativeRecoilPersistGate>
    </RecoilRoot>
  );
};
