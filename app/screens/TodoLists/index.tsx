/**
 * File: /screens/TodoLists/index.tsx
 * Project: app
 * File Created: 26-09-2022 01:23:49
 * Author: Ajith Kumar
 * -----
 * Last Modified: 10-10-2022 00:49:25
 * Modified By: Lavanya
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Header } from 'app/components/molecules/Header';
import { TodoListsBody } from '../../organisms/TodoListsBody';
import { Card } from '@native-theme-ui/core';
// import { Link } from "@risserlabs/solito/link";

export const TodoListsScreen = () => {
  return (
    <Card
      style={{
        backgroundColor: '#1F1F1E',
        alignItems: 'center',
        padding: 20,
        margin: 10,
      }}
    >
      <Header description="Store all your tasks">TodoLists</Header>
      <TodoListsBody />
    </Card>
  );
};
