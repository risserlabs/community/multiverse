/**
 * File: /screens/Manikanta/index.tsx
 * Project: app
 * File Created: 28-09-2022 23:05:15
 * Author: Manikanta
 * -----
 * Last Modified: 30-09-2022 00:10:34
 * Modified By: Manikanta
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Box, H1, Text } from '@native-theme-ui/core';
import React from 'react';
import { createParam } from '@risserlabs/solito/build';

const { useParam } = createParam();

export const ManikantaScreen = () => {
  const [manikantaColor] = useParam('manikantaColor');
  console.log('color', manikantaColor);

  return (
    <Box
      style={{
        alignItems: 'center',
      }}
    >
      <H1 style={{ backgroundColor: manikantaColor }}>
        My Name is Manikanta & my fav Color is<Text>&nbsp;</Text>
        {manikantaColor}
      </H1>
    </Box>
  );
};
