/**
 * File: /screens/TodoList/ToDOApp.stories.tsx
 * Project: app
 * File Created: 19-09-2022 07:41:15
 * Author: Ajith Kumar
 * -----
 * Last Modified: 20-09-2022 00:46:52
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { TodoListScreen } from '.';

export default {
  title: 'screens/TodoList',
  component: TodoListScreen,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <TodoListScreen />;
