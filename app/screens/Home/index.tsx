/**
 * File: /screens/Home/index.tsx
 * Project: app
 * File Created: 26-10-2022 10:05:04
 * Author: Clay Risser
 * -----
 * Last Modified: 01-12-2022 09:45:15
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, useCallback } from 'react';
import { Link } from '@risserlabs/solito/link';
import { useAnimationStateSx } from '@native-theme-ui/core';
import { Text, Flex, Button, Box } from 'app/components';

export const HomeScreen: FC<unknown> = () => {
  const animationState = useAnimationStateSx({
    from: {
      borderRadius: 9999,
      height: 100,
      width: 200,
      backgroundColor: 'primary',
    },
    to: {
      height: 100,
      width: 200,
      scale: 1,
      backgroundColor: 'secondary',
    },
    hover: {
      height: 200,
      width: 100,
      backgroundColor: 'primary',
    },
    jumble: {
      height: 500,
      width: 250,
      borderRadius: 150,
      backgroundColor: 'red',
    },
  });

  const handlePress = useCallback(() => {
    console.log('I was pressed!');
  }, []);

  return (
    <Flex sx={{ flexDirection: 'column' }}>
      <Text>Hello, multiplatform-framework!</Text>
      <Button onPress={handlePress}>Press Me</Button>

      <Link href="/lavanya">
        <Text>Go to Another Page</Text>
      </Link>
      <Link href="/todo-lists">
        <Text>Go To TodoLists</Text>
      </Link>
      <Link href="/todo-list/yellow">
        <Text>Go TodoList Name</Text>
      </Link>
      <Link href="/manikanta">
        <Text>Go to manikanta</Text>
      </Link>
      <Link href="/manikanta/blue">
        <Text>Go to Hello</Text>
      </Link>

      <Link href="/lavanya/blue">
        <Text>Click Me</Text>
      </Link>
      <Link href="/ajith">
        <Text>Ajith</Text>
      </Link>
      <Link href="/ajith/#E7291D">
        <Text>ajithColor</Text>
      </Link>
      <Flex
        sx={{
          alignItems: 'center',
          height: 200,
          justifyContent: 'center',
          width: 200,
        }}
      >
        <Box
          state={animationState}
          onHoverIn={() => animationState.transitionTo('jumble')}
          onHoverOut={() => animationState.transitionTo('to')}
          onPressIn={() => animationState.transitionTo('hover')}
          onPressOut={() => animationState.transitionTo('to')}
          // onLongPress={() => router.push("/about")}
          transition={{
            type: 'timing',
            duration: 300,
          }}
        />
      </Flex>
    </Flex>
  );
};
