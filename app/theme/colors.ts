/**
 * File: /theme/colors.ts
 * Project: app
 * File Created: 26-10-2022 07:46:46
 * Author: Clay Risser
 * -----
 * Last Modified: 02-12-2022 15:20:39
 * Modified By: K S R P BHUSHAN
 * -----
 * Promanager (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const colors = {
  textLightBlue: 'blue', // TODO: remove
  giftCard: {
    background: {
      '1': 'black',
      '2': 'cyan',
      '3': 'white',
    },
    text: {
      '1': 'green',
      '2': 'white',
      '3': 'green',
      '4': 'red',
    },
  },
  modal: {
    headerColor: {
      '1': '#F89880',
      '2': '#ADD8E6',
    },
    backgroundColor: {
      '1': '#BEBEBE',
    },
    text: {
      '1': 'black',
    },
  },
  walletMenuItem: {
    backgroundColor: {
      '1': '#F7E49F',
      '2': '#e8bcf0',
      '3': '#F6DCD5',
      '4': '#73ABC6',
    },
  },
  totalPriceBox: {
    backgroundColor: {
      '1': '#ececec',
      '2': '#000000',
    },
    text: {
      '1': 'blue',
      '2': '#ffffff',
    },
  },
  transactionLog: {
    text: {
      '1': 'green',
      '2': 'red',
    },
  },
};
