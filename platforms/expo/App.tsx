/**
 * File: /App.tsx
 * Project: @multiplatform/expo
 * File Created: 23-01-2022 02:18:40
 * Author: Clay Risser
 * -----
 * Last Modified: 02-12-2022 14:28:51
 * Modified By: K S R P BHUSHAN
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as SplashScreen from 'expo-splash-screen';
import React, { FC, useEffect } from 'react';
import fonts from 'app/fonts';
import { GlobalProvider } from 'app/provider';
import { NativeNavigation } from 'app/navigation/native';
import { Platform, View, StatusBar as RNStatusBar } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { useFonts } from 'expo-font';

const logger = console;
SplashScreen.preventAutoHideAsync().catch(logger.error);

const App: FC<unknown> = () => {
  const [fontsLoaded] = useFonts(fonts);

  useEffect(() => {
    if (fontsLoaded) SplashScreen.hideAsync();
  }, [fontsLoaded]);
  if (!fontsLoaded) return null;

  function renderStatusBar() {
    if (Platform.OS === 'android') {
      return (
        <>
          <StatusBar />
          <View style={{ height: RNStatusBar.currentHeight }} /> 
        </>
      );
    }
    return <StatusBar />;
  }

  return (
    <GlobalProvider>
      <View style={{ flex: 1 }}>
        {renderStatusBar()}
        <NativeNavigation />
      </View>
    </GlobalProvider>
  );
};

export default App;
