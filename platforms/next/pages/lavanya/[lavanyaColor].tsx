/**
 * File: /pages/lavanya/[lavanyaColor].tsx
 * Project: @multiplatform/next
 * File Created: 29-09-2022 23:28:05
 * Author: Lavanya
 * -----
 * Last Modified: 30-09-2022 02:16:34
 * Modified By: Lavanya
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { NextPage } from 'next';
import React from 'react';

import { LavanyaScreen } from 'app/screens/Lavanya';

const LavanyaColor: NextPage = () => <LavanyaScreen />;
export default LavanyaColor;
